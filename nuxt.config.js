const nodeExternals = require('webpack-node-externals')

const resolve = dir => require('path').join(__dirname, dir)

module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'POWERPARTNERS',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'New frontend for powerpartners site',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons',
      },
    ],
  },
  plugins: ['~/plugins/vuetify.js', '~/plugins/axios', '~/plugins/vuelidate'],
  modules: ['@nuxtjs/axios', '@nuxtjs/toast'],
  axios: {
    baseURL: 'http://alpha.powerpartners.ru/api/v2.0',
    browserBaseURL: 'http://alpha.powerpartners.ru/api/v2.0',
  },
  toast: {
    position: 'top-center',
  },

  rules: {
    'max-len': 'off',
    'linebreak-style': 0,
  },

  css: ['~/assets/style/app.styl'],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    buildDir: 'build',
    cssSourceMap: false,
    babel: {
      plugins: [
        [
          'transform-imports',
          {
            vuetify: {
              transform: 'vuetify/es5/components/${member}',
              preventFullImport: true,
            },
          },
        ],
      ],
    },
    vendor: [
      '~/plugins/vuetify.js',
      'axios',
      'vuelidate',
      'vue-toasted',
      'vue2-datepicker',
      'moment',
    ],
    extractCSS: true,
    /*
    ** Run ESLint on save
    */
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
      if (ctx.isServer) {
        config.externals = [
          nodeExternals({
            whitelist: [/^vuetify/],
          }),
        ]
      }
    },
  },
}
