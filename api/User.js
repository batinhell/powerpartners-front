import axios from 'axios'
import store from '@/store'

export default {
  login(credentials) {
    return axios.post('/auth', credentials).then(r => r.data)
  },

  get(id) {
    const params = Object.assign(
      { token: store.getters.token },
      id ? { user_id: id } : {},
    )

    return axios.post('/users/info', params).then(r => r.data)
  },
}
