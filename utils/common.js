export const STATUSES = {
  ok: 'Принят',
  missed: 'Пропущенный',
  runaway: 'Сорвавшийся',
}

export const responseErrorInterceptor = response => {
  if (response.data.errors) {
    throw response.data.errors[0]
  }

  return response
}

export const menuItems = [
  {
    icon: 'timeline',
    title: 'Статистика',
    access: ['affiliate', 'manager', 'seller'],
    items: [
      {
        title: 'Заказы',
        url: '/stat.php',
        access: ['affiliate', 'manager'],
        old: true,
      },
      {
        title: 'Звонки',
        url: '/calls',
        access: ['affiliate', 'manager'],
      },
      {
        title: 'Эффективность',
        url: '/effect.php',
        access: ['manager'],
        old: true,
      },
      {
        title: 'Анализ продаж',
        url: '/report-sales.php',
        access: ['manager'],
        privileges: ['senior'],
        old: true,
      },
      {
        title: 'Фин. отчет',
        url: '/d/accounting-report.php',
        access: ['manager', 'seller'],
        privileges: ['finance'],
        old: true,
      },
      {
        title: 'Рефералы',
        url: '/referals.php',
        access: ['affiliate'],
        old: true,
      },
      {
        title: 'Платежи',
        url: '/history.php',
        access: ['affiliate', 'manager'],
        old: true,
      },
    ],
  },
  {
    icon: 'shopping_cart',
    title: 'Заказы',
    access: ['manager', 'seller', 'courier'],
    items: [
      {
        title: 'Список',
        url: '/d/orders.php',
        old: true,
      },
      {
        title: 'Композиты',
        url: '/doc/composite/',
      },
      {
        title: 'Доставки',
        url: '/d/delivery.php',
        old: true,
      },
      {
        title: 'Самовывозы',
        url: '/d/pickup.php',
        old: true,
      },
      {
        title: 'Счета',
        url: '/d/wire.php',
        old: true,
      },
      {
        title: 'Предзаказы',
        url: '/d/pre-order.php',
        old: true,
      },
      {
        title: 'Остатки',
        url: '/d/stock.php',
        access: ['manager'],
        old: true,
      },
      {
        title: 'Новый заказ',
        url: '/d/order.php',
        access: ['manager', 'seller'],
        old: true,
      },
    ],
  },
  {
    icon: 'build',
    title: 'Управление',
    access: ['affiliate', 'manager'],
    items: [
      {
        title: 'Заказы',
        url: '/orders.php',
        access: ['manager'],
        old: true,
      },
      {
        title: 'Наличие и цены',
        url: '/reserv.php',
        access: ['manager'],
        privileges: ['senior'],
        old: true,
      },
      {
        title: 'Базовые цены',
        url: '/price-list-base.php',
        access: ['manager'],
        privileges: ['finance'],
        old: true,
      },
      {
        title: 'Магазины',
        url: '/sites.php',
        access: ['affiliate'],
        old: true,
      },
      {
        title: 'Новый заказ',
        url: '/stores.php',
        access: ['affiliate'],
        old: true,
      },
      {
        title: 'Партнеры',
        url: '/affiliates.php',
        access: ['admin'],
        old: true,
      },
    ],
  },
  {
    icon: 'info',
    title: 'Информация',
    showAlways: true,
    items: [
      {
        title: 'Документация',
        url: '/doc/',
        old: true,
      },
      {
        title: 'Вопросы и ответы',
        url: '/faq.php',
        old: true,
      },
      {
        title: 'Форум',
        url: '/forum/',
        old: true,
      },
      {
        title: 'Тех. поддержка',
        url: '/support.php',
        old: true,
      },
    ],
  },
]
