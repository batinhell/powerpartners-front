import { responseErrorInterceptor } from '@/utils/common'

export default function ({ $axios }) {
  $axios.onResponse(responseErrorInterceptor)
}
