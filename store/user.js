import Vue from 'vue'

export const state = () => ({
  profile: {},
})

export const getters = {
  profile(state) {
    return state.profile
  },
  isProfileLoaded(state) {
    return !!state.profile.user_id
  },
}

export const mutations = {
  update(state, user) {
    Vue.set(state, 'profile', user)
  },
}

export const actions = {
  load({ commit, rootState, dispatch }) {
    const params = { token: rootState.auth.token }

    return this.$axios
      .$post('/users/info', params)
      .then(({ user }) => {
        commit('update', user)
      })
      .catch(error => {
        dispatch('auth/logout', null, { root: true })
      })
  },
}
