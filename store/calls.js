/* eslint camelcase: ["error", {properties: "never"}] */
import Vue from 'vue'

export const state = () => ({
  calls: [],
  details: [],
})

export const getters = {
  calls: state => state.calls,
  details: state => state.details,
}

export const mutations = {
  update(state, calls) {
    state.calls = calls
  },
  setDetails(state, details) {
    state.details = details
  },
}

export const actions = {
  load({ commit, rootState }, { from, to }) {
    const params = { token: rootState.auth.token, from, to }

    return this.$axios.$post('/calls/list', params).then(({ calls }) => {
      commit('update', calls)
    })
  },
  // eslint-disable-next-line
  details({ commit, rootState }, { call_id }) {
    const params = { token: rootState.auth.token, call_id }

    return this.$axios.$post('/calls/details', params).then(({ details }) => {
      commit('setDetails', details)
    })
  },
}
