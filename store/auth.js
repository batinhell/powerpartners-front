import Cookie from 'js-cookie'

export const state = () => ({
  token: '',
  status: '',
  error: '',
})

export const mutations = {
  setToken(state, token) {
    state.token = token
  },
  token: state => state.token,
}

export const actions = {
  login({ commit, dispatch }, credentials) {
    return this.$axios.$post('/auth', credentials).then(response => {
      setCookie(response, credentials.rememberMe)
      commit('setToken', response.token) // mutating to store for client rendering

      // load user profile
      return dispatch('user/load', null, { root: true })
    })
  },

  logout({ commit }) {
    return new Promise(resolve => {
      commit('setToken', null)
      commit('user/update', {}, { root: true })
      Cookie.remove('token')
      Cookie.remove('affid')
      Cookie.remove('security')
      resolve()
    })
  },
}

function setCookie({ token, legacy }, rememberMe) {
  const domain = `.${BASE_URL()}`
  const today = new Date()
  // eslint-disable-next-line
  const expireDate = new Date(today.getTime() + 365 * 24 * 60 * 60 * 1000)
  const expire = rememberMe ? `expires=${expireDate.toGMTString()};` : ''
  const fullToken = `${token};${expire}path=/;domain=${domain}`

  Cookie.set('token', fullToken) // saving full token in cookie for compatibility
  Cookie.set('authToken', token) // saving auth token in cookie for server rendering

  if (legacy) {
    const affid = `${legacy.affid};${expire}path=/;domain=${domain}`
    const security = `${token};${expire}path=/;domain=${domain}`

    Cookie.set('affid', affid)
    Cookie.set('security', security)
  }
}

function BASE_URL() {
  let { domain } = document
  domain = domain.replace(/^www\./i, '')
  domain = domain.replace(/^d\./i, '')
  return domain
}
