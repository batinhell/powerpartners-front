const cookieparser = require('cookieparser')
const axios = require('axios')

export const state = () => ({
  sidebar: false,
})

export const mutations = {
  toggleSidebar(state) {
    state.sidebar = !state.sidebar
  },
}

export const actions = {
  nuxtServerInit({ commit, dispatch }, { req }) {
    const cookies = cookieparser.parse(req.headers.cookie || '')

    if (cookies.authToken) {
      commit('auth/setToken', cookies.authToken)
      return dispatch('user/load')
    }

    commit('auth/setToken', null)
    return Promise.resolve()
  },
}
