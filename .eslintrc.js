module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 2017,
    sourceType: 'module',
  },
  env: {
    browser: true,
    node: true,
  },
  plugins: ['html'],
  extends: ['airbnb-base', 'plugin:vue/recommended'],
  settings: {
    'import/resolver': {
      webpack: {
        config: {
          resolve: {
            extensions: ['.js', '.vue', '.json'],
            alias: {
              '@': __dirname,
              '~': __dirname,
            },
          },
        },
      },
    },
  },
  // add your custom rules here
  rules: {
    // don't require .vue extension when importing
    'import/extensions': [
      'error',
      'always',
      {
        js: 'never',
        vue: 'never',
      },
    ],
    // disallow reassignment of function parameters
    // disallow parameter object manipulation except for specific exclusions
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: [
          'state', // for vuex state
          'acc', // for reduce accumulators
          'e', // for e.returnvalue
        ],
      },
    ],
    // allow optionalDependencies
    'import/no-extraneous-dependencies': 'off',
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/max-attributes-per-line': 'off',
    'vue/attributes-order': 'off',
    'vue/html-self-closing': 'off',
    'no-shadow': ['error', { allow: ['state'] }],
    'arrow-parens': ['error', 'as-needed'],
    'no-return-assign': 'off',
    'arrow-body-style': 'off',
    'no-console': 'off',
    'no-unused-vars': 'off',
    semi: ['error', 'never'],
    camelcase: 'off',
    'max-len': 'off',
<<<<<<< HEAD
    'no-use-before-define': ['error', { functions: false }],
    'no-restricted-globals': 'off',
=======
    'linebreak-style': 0,
>>>>>>> d467dbcb0672d700b4214ea3e70e05fb354440de
  },
  globals: {},
}
